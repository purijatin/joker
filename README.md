# Joker

## About 

JOKER is a *stateful* mocking framework. It uses [ZoomBA](https://gitlab.com/non.est.sacra/zoomba) language to define the behaviour of the mock objects in a definition file for the mocks. 
Using this, we have experienced a code reduction of 3/4 times from raw use of [Mockito](http://site.mockito.org). 
Yes, you can code fully in Joker definitions, instead of verbose, weird wiring like : `when().thenXXX()` syntaxes.

And yes, ZoomBA is Turing Complete, and lets you code freely and with much less lines. And no, ZoomBA is very weakly typed, but more type aware than Groovy or ECMA Scripts. 

Joker is about declaration driven *stateful* mocking.

## Why The Name Joker ?

JOKER is an acronym stemming from : 
> Justified Open Knowledge Evaluation and Ramification.

Like the proverbial joker card can become anything you want it to be.
Used for true mocking objects for state maintenance and modelling behaviour for unit / integration testing.
The name also faithfully serves as a reminder to the goals : what it wants it aspires to be :-

* Joker card : one can use a *joked* object as a substitute for the real one.
* The strategy of separating specification from usage is *Justifiable* 
* Joker is *Open* as it would be - one can change default behaviour anytime 
* The *Knowledge Evaluation* is externalised, hence one can choose to change the specification any time - w/o any code change. 
* The *Ramification* comes from the fact that Joker allows *branching*:
  *  function call to *unknown* or *unspecified* methods produces an error
  *  User can specify logical branching 
  *  One can conditionally raise errors  
* Writing mocking specification should be a simple and fun activity. Joker reminds you that. 

The mocked objects in Joker are true mocks, they are not stubs, at all.
Hence, *Joker*.      

## Motivation 

It is the obscenely high investments of mocking. End user wants the product to work. How many lines are written to get the job done is immaterial for any purpose. And, moreover, none of the current mocking technology have no states! That ensures that there would be problem when mocking an object that has hidden states, IO, Lists, Iterators, external web services, all comes under this. Stateless objects like Registries are outside of the scope, 
Mockito beautifully solves those stateless problems.

It is managing states, is what Joker is designed for. 

## Usage 

### Binary 
As of now, build from the source.
Alternatively, one can download the [binary](https://gitlab.com/non.est.sacra/joker/blob/master/dist/joker.zip).
We are working hard to put all of these into maven repository.
It may take some time.

### Maven Configuration 

Here is how one can use it:

```xml
<dependency>
  <groupId>org.zoomba</groupId>
  <artifactId>joker.core</artifactId>
  <version>0.1-SNAPSHOT</version>
</dependency>
```
as one can see it is a sister project of ZoomBA.

### Definition File 
The definition of the Mock is a ZoomBA script : say Basic.zm :

    /*
      Defines various Joking stuff
      Read more about the ZoomBA lang:
      https://gitlab.com/non.est.sacra/zoomba/wikis/11-oops
    */
    // A Mock for List interface
    def JListMock : {
       $TYPE : 'java.util.List', // class type of the mock 
       $$ : def(){ $.u = list() }, // the constructor 
       add : def(o){ $.u.add(o) }, //  add() method 
       get : def(i){ $.u[i] }, // specification of get() 
       size : def(){ size($.u) } // size() function
    }
    
    // A mock for Iterator
    def JIteratorMock : {
      $TYPE : 'java.util.Iterator',
      // capable of taking constructor args 
      $$ : def( max_calls ){ $.num_calls = -1
                    $.max_calls = max_calls }, // hidden states 
      hasNext : def(){ ( $.num_calls +=1 ) < $.max_calls }, // cool?
      next : def() { random(100) } // returns some random data 
    }

One can see how ZoomBA is used to manage and maintain states.
You would see the difference between vanilla Mockito and Joker, once you see the Java Code.

### Call from JVM Code 
Once you have the definition file, you can now code as if you are using an enhanced version of the Mockito itself :

```java
/* from the test file of Joker itself */
package joker.core.tests;

import joker.core.Joker;
import org.junit.Test;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;

public class JTest {
    private static final String DEFINITION_PATH = "definitions/Basic.zm";
    static final String LIST_MOCK = "JListMock" ;
    static final String ITER_MOCK = "JIteratorMock" ;
    @Test
    public void jokeList(){
        // conventional 
        List l = Joker.joke( LIST_MOCK , DEFINITION_PATH );
        assertNotNull(l);
        assertEquals(0, l.size() ) ;
        l.add(42); // it would add, actually 
        // cool? Automatic state management? 
        assertEquals(1, l.size() ) ; // under plain Mockito : 0
        try{
            l.contains(42); // under plain Mockito : NOP
            // as this joke is not defined, it must throw exception!
            assertTrue(false);
        }catch (Throwable t){
            // and yes, we must have t
            assertNotNull(t);
        }
    }
    @Test
    public void jokeIterator(){
        final int max = 3;
        // jokes can take constructor arguments 
        Iterator i = Joker.joke( ITER_MOCK, DEFINITION_PATH, max);
        int count = 0;
        while ( i.hasNext() ){ i.next(); count++; }
        // insanely simple, is not it?
        assertEquals(max, count );
    }
}
```
You can not make it simpler. Trust us, we tried.

## A Complex Situation in Mockito
Observe the link in [Stack Overflow](http://stackoverflow.com/questions/2684630/how-can-i-make-a-method-return-an-argument-that-was-passed-to-it).
The goal was to mock a service that persists Objects and can return them by their name. This is a tell tale situation of why Joker was built.
The corresponding Mockito Java code is here:

```java
RoomService roomService = mock(RoomService.class);
final Map<String, Room> roomMap = new HashMap<String, Room>();

// mock for method persist
doAnswer(new Answer<Void>() {
    @Override
    public Void answer(InvocationOnMock invocation) throws Throwable {
        Object[] arguments = invocation.getArguments();
        if (arguments != null && arguments.length > 0 && arguments[0] != null) {
            Room room = (Room) arguments[0];
            roomMap.put(room.getName(), room);
        }
        return null;
    }
}).when(roomService).persist(any(Room.class));

// mock for method findByName
when(roomService.findByName(anyString())).thenAnswer(new Answer<Room>() {
    @Override
    public Room answer(InvocationOnMock invocation) throws Throwable {
        Object[] arguments = invocation.getArguments();
        if (arguments != null && arguments.length > 0 && arguments[0] != null) {
            String key = (String) arguments[0];
            if (roomMap.containsKey(key)) {
                return roomMap.get(key);
            }
        }
        return null;
    }
});
```
followed by the actual testing code:

```java
String name = "room";
Room room = new Room(name);
roomService.persist(room);
assertThat(roomService.findByName(name), equalTo(room));
assertNull(roomService.findByName("none"));
``` 
### Joker Way
In Joker, you keep the Java code almost same:

```java
interface DataPersistService{
    Dummy load(String name);
    boolean save( Dummy d , String name );
}
// the test code:
@Test
public void showDifference(){
    DataPersistService s = Joker.joke( PERSISTENCE_MOCK, 
          DEFINITION_PATH );
    final String name = "x" ;
    Dummy d = new Dummy();
    d.setSomeThing(42);
    assertTrue( s.save( d , name) );
    d = s.load(name);
    assertNotNull( d );
    assertEquals( 42, d.getSomeThing() );
    assertNull( s.load("none") );
}
``` 
And finally, the ZoomBA definition, which is the *fake* :

```
def JDataPersistServiceMock : {
  $TYPE : 'joker.core.tests.JTest$DataPersistService',
  load : def( name ) { (name @ $ ) ? $[name] : null },
  save : def( dummy, name ){ $[name] = dummy ; true }
}
``` 

This shows how Joker should actually be used, and where preferrably.


## Passing Defaults 

You probably want to use *Mockito* like defaults. You can:

```
E1 = new ( 'java.lang.Exception' , 'I am Joker raised Exception' )
// A Mock for List interface
def JListMock : {
   $TYPE : 'java.util.List',
   $$ : def(){ $.u = list() },
   add : def(o){ $.u.add(o) },
   get : def(i){ $.u[i] },
   size : def(){ size($.u) },
   $DEFAULTS : {
      "get" : [ { 42 : 0 } , { 55 : 42 } ],
      "!get" : [ { 100 : E1 } ]
   }
}
```
Note that the *$DEFAULTS* has the mapping of arguments into values.
It reads :
The *get* method when supplied with :
  * argument 42, should return 0.
  * argument 55, should return 42.

The *!get* syntax means, *get* will *throw exception* *E1*.
Given 100, it would throw the exception.
The corresponding test code :

```java
@Test
public void testDefaultOptions(){
    List l = Joker.joke( LIST_MOCK , DEFINITION_PATH );
    assertEquals( 0 , l.get(42)  );
    assertEquals( 42 , l.get(55)  );
    // check exceptions ?
    try{
        l.get(100); // should throw exception
        assertTrue(false);
    }catch (Throwable t){
        assertTrue(t.getMessage().contains("Joker"));
    }
}
```
## Programmatic Access 
While we do not recommend it, there is programmatic access - the Mockito way. Albeit our is simpler and poorer.

```
// A mock to test programmatic
def JStringMock : {
  $TYPE : 'zoomba.lang.core.types.ZString'
}
```
The mock above only defines the binding. To add behaviour into it :

```java
@Test
public void testStringMock(){
    ZString s = Joker.joke( STRING_MOCK, DEFINITION_PATH );
    Joker.on(s).calling("charAt").with(0).returns('J');
    Joker.on(s).calling("charAt").with(1)
      .raises( new IndexOutOfBoundsException("1"));
    assertEquals( 'J', s.charAt(0) ); // we are good.
    try{
        s.charAt(1); // throw it off 
        assertTrue(false);
    }catch (IndexOutOfBoundsException i){
        assertTrue(true); // the very exception we wanted 
    }
}
```


## Supplements

Joker was designed using TDD. Here is how the code coverage looks like:
![Coverage of Joker](https://gitlab.com/non.est.sacra/joker/raw/master/cc.png)

You probably want to read more about [ZoomBA](https://gitlab.com/non.est.sacra/zoomba/wikis/home)
and [Object Orientation in ZoomBA](https://gitlab.com/non.est.sacra/zoomba/wikis/11-oops).
  A Joke definition is a ZoomBA object, mocking for a real Java one.
As usual, we are trying to come up with a full fledged manual on ZoomBA and Joker, both.
We guess that it would probably be *Using Joker through ZoomBA*. :-)


