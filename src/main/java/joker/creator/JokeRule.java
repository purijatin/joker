package joker.creator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class JokeRule {

    private static final Map<Object, MethodHook> hooks = new ConcurrentHashMap<>();

    public static void addJoke( Object o, MethodHook hook ){
        hooks.put( o, hook );
    }

    Object object;

    String methodName;

    Object[] arguments;

    Object returnValue;

    Throwable except;

    public int times(String methodName){
        if ( object == null ) throw new UnsupportedOperationException("Object instance is not set!");
        MethodHook hook = hooks.get( object );
        return  hook.numCalls(methodName);
    }

    public Object args(String methodName){
        if ( object == null ) throw new UnsupportedOperationException("Object instance is not set!");
        MethodHook hook = hooks.get( object );
        Object[] args = hook.args(methodName);
        return ( args != null && args.length == 1) ? args[0] : args ;
    }

    public boolean called(String methodName){
        try {
            return  0 != times(methodName);
        }catch (Exception e){}
        return false;
    }

    public JokeRule(){
        object = null;
        methodName = "" ;
        arguments = null ;
        returnValue = NIL;
        except = null ;
    }

    public JokeRule on(Object o){
        if ( !hooks.containsKey(o) ) throw new UnsupportedOperationException("This Object is not Joked!");
        object = o ;
        return this;
    }

    public JokeRule calling(String method){
        if ( object == null ) throw new UnsupportedOperationException("Object instance is not set!");
        methodName = method ;
        return this;
    }

    public JokeRule with(Object...args){
        if ( methodName.isEmpty() ) throw new UnsupportedOperationException("Method is not set!");
        arguments = args ;
        return this;
    }

    public void returns(Object r){
        if ( arguments == null ) throw new UnsupportedOperationException("Arguments are not set!");
        returnValue = r ;
        except = null ;
        apply();
    }

    public void raises(Throwable e){
        if ( methodName.isEmpty() ) throw new UnsupportedOperationException("Method is not set!");
        except = e ;
        returnValue = NIL;
        apply();
    }

    private void apply(){
        MethodHook hook = hooks.get( object );
        hook.applyRule(this);
    }

}
