package joker.creator;

import org.mockito.cglib.proxy.MethodInterceptor;
import org.mockito.cglib.proxy.MethodProxy;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZTypes;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class MethodHook implements MethodInterceptor {

    private static final String INTERPRETER = "1NT" ;

    private static final String HASH_CODE = "0HC" ;

    private static final String HASH_CODE_METHOD = "hashCode" ;

    private static final String DEFAULTS = "$DEFAULTS" ;

    private static final String  METHOD_COUNT_PREFIX = "#" ;

    private static final String  METHOD_ARGS_PREFIX = "@@" ;


    // Black Joker https://en.wikipedia.org/wiki/Playing_cards_in_Unicode
    private static final String SEP = "\uF0CF" ;

    private ZObject proto;

    private ZInterpret interpret;

    private Map<String,Map<String,Object>> defaults ;

    private void populateDefaults(){
        defaults = new HashMap<>();
        if ( !proto.containsKey( DEFAULTS ) ) return;
        Map m = (Map)proto.get( DEFAULTS );
        for (Object key : m.keySet() ){
            Map<String,Object> methodMap = new HashMap<>();
            List l = (List)m.get(key);
            for ( Object entry : l ){
                Map res = (Map)entry;
                for ( Object k : res.keySet() ){
                    String keyString ;
                    if ( k instanceof List ){
                        keyString = ZTypes.string( (List)k , SEP );
                    }else{
                        keyString = ZTypes.string( k );
                    }
                    methodMap.put( keyString , res.get(k) );
                }
            }
            defaults.put( String.valueOf(key) , methodMap );
        }
    }

    public MethodHook(ZInterface.ZProtoType definition , Object[] constructorArgs ){
        interpret = new ZInterpret( definition.script );
        interpret.prepareCall( definition.script.getExternalContext() );
        // construct object...
        proto = ZObject.createFrom( definition.proto , interpret, constructorArgs );
        proto.put( HASH_CODE, (int)System.nanoTime() );
        proto.put( INTERPRETER, interpret );
        populateDefaults();
    }

    private Object extractDefault( String methodName, Object[] args ) throws Throwable {
        Map<String,Object> values = defaults.get( methodName );
        boolean raiseException = false ;
        if ( values == null ){
            values = defaults.get( "!" + methodName );
            if ( values == null ) return NIL;
            raiseException = true ;
        }

        String paramKey = ZTypes.string( new ZArray( args, false) , SEP ) ;
        if ( !values.containsKey( paramKey ) ){
            // perhaps exception will be found ?
            values = defaults.get( "!" + methodName );
            if ( values == null || !values.containsKey( paramKey )) return NIL;
            raiseException = true ;
        }
        Object ret = values.get( paramKey );
        System.err.printf("Passing defaults : %s(%s)->(%s) [is not a good idea...]\n",
                methodName, paramKey, ret );
        if ( raiseException ){
            throw (Throwable)ret;
        }
        return ret ;
    }

    void applyRule(JokeRule rule){
        String paramKey = ZTypes.string( new ZArray( rule.arguments , false) , SEP ) ;
        boolean exceptionRule = ( rule.except != null ) ;
        String methodName = ( rule.except != null ) ?  "!" + rule.methodName : rule.methodName ;
        Map<String,Object> m = defaults.get( methodName );
        if ( m == null ){
            m = new HashMap<>();
            defaults.put( methodName, m );
        }
        m.put( paramKey, exceptionRule ? rule.except : rule.returnValue );
    }

    private void storeInvocationData(String methodName, Object[] args){
        String numCount = METHOD_COUNT_PREFIX + methodName ;
        String argStore = METHOD_ARGS_PREFIX + methodName ;
        proto.put( argStore, args );
        Integer count = (Integer)proto.get( numCount );
        if ( count == null ){
            proto.put( numCount , 1 );
        }else{
           proto.put( numCount, count + 1 );
        }
    }

    int numCalls(String methodName){
        String numCount = METHOD_COUNT_PREFIX + methodName ;
        Integer count = (Integer)proto.get( numCount );
        if ( count == null ){
            return 0 ;
        }
        if ( HASH_CODE_METHOD.equals(methodName) ){
            return count -1 ;
        }
        return count;
    }

    Object[] args(String methodName){
        return (Object[]) proto.get( METHOD_ARGS_PREFIX + methodName ) ;
    }

    @Override
    public Object intercept(Object obj, Method method,
                            Object[] args, MethodProxy proxy) throws Throwable {
        // we do not support overload by now... just don't
        String methodName = method.getName();
        storeInvocationData( methodName , args);

        if ( methodName.equals(HASH_CODE_METHOD) ){
            return proto.get( HASH_CODE );
        }
        try {
            Function.MonadicContainer mc = proto.intercept(null, interpret, methodName,
                    args, null, null);
            if (mc.isNil()) {
                if (!Void.TYPE.equals(method.getReturnType())) {
                    throw new RuntimeException("Method is not properly defined!");
                }
            }
            Object ret = mc.value();
            if ( ret == proto ){ // the ZoomBA code returned $, hence return actual object
                return obj;
            }
            return ret;
        }catch (Throwable t){
            // check fall back
            Object fallBack = extractDefault( methodName, args );
            if ( !NIL.equals( fallBack ) ) {
                return fallBack ;
            }
            throw t;
        }
    }
}
